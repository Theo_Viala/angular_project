import { Component, OnInit, Input } from '@angular/core';
import { HTTPService } from '../http.service'

@Component({
  selector: 'app-list-nom',
  templateUrl: './list-nom.component.html',
  styleUrls: ['./list-nom.component.css']
})
export class ListNomComponent implements OnInit {

  constructor(private HTTPService: HTTPService) { }

  ngOnInit(): void {
  }

  @Input() user: any
  detailUser: any;

  ngOnChanges() {
    console.log(JSON.parse(this.user))

    this.detailUser = JSON.parse(this.user)
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListNomComponent } from './list-nom.component';

describe('ListNomComponent', () => {
  let component: ListNomComponent;
  let fixture: ComponentFixture<ListNomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListNomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListNomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

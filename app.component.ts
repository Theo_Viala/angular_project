import { Component } from '@angular/core';
import { HTTPService } from './http.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Projet Aleksandr/Theo';
  display="block";
  flex ="inline-flex"; 

  Verifclick = false;
  VerifDonnee = false;
  users: any;
  detailUser: any;

  constructor(private HTTPService: HTTPService) { }

  InitList() {
    this.HTTPService.RecupPosts().subscribe(data => {
      this.users = data;
      this.VerifDonnee = true;
    });
  }

  listdetail(user: any) {
    this.detailUser = JSON.stringify(user);
    this.Verifclick = true;
  }
}
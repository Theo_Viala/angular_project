import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HTTPService } from './http.service';
import { ListNomComponent } from './list-nom/list-nom.component';

@NgModule({
  declarations: [
    AppComponent,
    ListNomComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [HTTPService],
  bootstrap: [AppComponent]
})
export class AppModule { }

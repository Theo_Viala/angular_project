import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class HTTPService {
  private wsUrl: string = "https://jsonplaceholder.typicode.com/users"

  constructor(private http: HttpClient) { }

  RecupPosts() {
    return this.http.get(this.wsUrl);
  }
}
